import tensorflow as tf
from tensorflow.python.framework import function
import time

with tf.device('/cpu:0'):
    fib = function.Declare("Fib", [("n", tf.int32)], [("ret", tf.int32)])

    @function.Defun(tf.int32, func_name="Fib", out_names=["ret"])
    def FibImpl(n):
      return tf.cond(tf.less_equal(n, 1),
                     lambda: tf.constant(1),
                     lambda: fib(n - 1) + fib(n - 2))

    FibImpl.add_to_graph(tf.get_default_graph())

    n = tf.placeholder(tf.int32, shape=[])
    result = fib(n)

with tf.Session() as sess:
    #print (sess.run(c))
    #print(sess.run(result, feed_dict={n: 0}))  # 1

    for a in range(50):
        t0 = time.time()
        num = sess.run(result, feed_dict={n: a})
        t1 = time.time()

        total = t1 - t0
        print (a, num, total)