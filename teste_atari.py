import time
import gym

# env = gym.make('Breakout-v0')
# env = gym.make('MsPacman-v0')
env = gym.make('Pong-v0')

num_episodes = 2000
num_timestep = 200

# for i_episode in range(20):
for episode in range(num_episodes):

    observation = env.reset()

    print("EPISODE: ", episode)
    print("observation: ", observation)

    for timestep in range(num_timestep):
        env.render()
        action = env.action_space.sample()

        print(action)

        observation, reward, done, info = env.step(action)

        # print ("EPISODE: ", episode, ".", timestep)
        # print ("observation: ", observation)
        # print ("reward: ", reward)
        # print ("done: ", done)
        # print ("info: ", info)
        # print ("")

        # time.sleep(.05)
        if done:
            print("Episode finished after {} timesteps".format(timestep + 1))
            break
