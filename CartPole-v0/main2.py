import time
import numpy as np
import gym

def main():
    env = gym.make('CartPole-v0')

    #parameters = [-0.42286364, 0.72913952, -0.35912646, 1.2089423 ]



    num_episodes = 2000
    episodes_per_update = 2
    num_timestep = 200

    noise_scaling_base = 0.1
    noise_scaling = 0.1
    parameters = np.random.rand(4) * 2 - 1
    bestreward = 0


    #for i_episode in range(20):
    for episode in range(num_episodes):

        observation = env.reset()

        newparams = parameters + (np.random.rand(4) * 2 - 1) * noise_scaling
        #totalreward = 0

        print ("EPISODE: ", episode)
        print (")Test params: ", newparams)

        episodeRewards = []

        for update in range(episodes_per_update):
            reward = run_episode(env, newparams, num_timestep)
            episodeRewards.append(reward)
            print ("reward[" + str(update) + "]: ", reward)

        meanreward = np.mean(episodeRewards)
        print ("meanreward: ", meanreward)

        print ("Magic params old: ", parameters)

        if meanreward > bestreward:
            noise_scaling = noise_scaling_base
            bestreward = meanreward
            parameters = newparams
            # considered solved if the agent lasts 200 timesteps
            #if reward == 200:
             #   break
        elif bestreward > meanreward:
            noise_scaling *= (1.1)

        print ("Magic params new: ", parameters)
        print ("")




def run_episode(env, parameters, steps):

    observation = env.reset()
    totalreward = 0

    for timestep in range(steps):
        #print ("timestep: ", timestep)

        env.render()

        matmul = np.matmul(parameters, observation)

        action = 0 if matmul < 0 else 1

        #print("left" if action == 0 else "right", matmul)

        observation, reward, done, info = env.step(action)

        totalreward += reward

        # print ("observation: ", observation)
        # print ("reward: ", reward)
        # print ("done: ", done)
        # print ("info: ", info)
        # print ("")

        time.sleep(.006)
        if done:
            break

    #print("Episode finished after {} timesteps".format(timestep + 1))
    return totalreward


main()